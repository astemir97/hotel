FROM python:3.11.4-slim-bullseye
ENV FLASK_APP=/opt/project/run.py
WORKDIR /opt/project
COPY . .
EXPOSE 5000
RUN pip3 install -r requirements.txt
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
